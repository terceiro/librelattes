class SearchController < ApplicationController

  def index
    if params[:q]
      @curriculums = Curriculum.search(params[:q])
      if params[:feeling_lucky]
        redirect_to curriculum_path(@curriculums.first)
      end
    else
      @curriculum_count = Curriculum.count
    end
  end

end
