class CurriculumsController < ApplicationController

  def index
    redirect_to action: 'new'
  end

  def show
    @curriculum = Curriculum.find(params[:id])
    respond_to do |format|
      format.html
      format.xml { send_data @curriculum.sanitized_data, content_type: 'application/xml', disposition: 'attachment' }
      format.json { render json: @curriculum }
    end
  end

  def new
    @curriculum = Curriculum.new
  end

  def create
    @curriculum = Curriculum.build(params[:data])
    @curriculum.save!
  rescue
    render action: 'new'
  end

end
