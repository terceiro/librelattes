class DocsController < ApplicationController

  def show
    render action: params[:id].underscore
  end

end
