require 'nokogiri'
require 'magic'
require 'stringio'
require 'tmpdir'

class Curriculum < ActiveRecord::Base

  validates_uniqueness_of :identifier

  def self.build(data)
    self.new.tap do |curriculum|
      curriculum.send :read!, data
    end
  end

  def self.search(query)
    # FIXME THIS IS HORRIBLE. Use full text search instead!
    like_expr = ['%', query.downcase, '%'].join
    self.where(['lower(name) LIKE ?', like_expr])
  end

  def data
    self[:data]
  end
  private :data

  def sanitized_data
    doc = Nokogiri::XML(data)

    doc.css('ENDERECO-RESIDENCIAL').remove
    d = doc.css('DADOS-GERAIS')
    [
      'NOME-DO-PAI',
      'NOME-DA-MAE',
      'DATA-NASCIMENTO',
      'CPF',
      'SEXO',
      'RACA-OU-COR',
      'NUMERO-IDENTIDADE',
      'ORGAO-EMISSOR',
      'UF-ORGAO-EMISSOR',
      'DATA-DE-EMISSAO',
      'NUMERO-DO-PASSAPORTE',
    ].each do |attr|
      d.attribute(attr).remove
    end

    doc.to_s
  end

  protected

  def read!(io)
    self.data = io.read

    filename = io.original_filename
    mime_type = Magic.guess_string_mime(data)
    if filename =~ %r{\.xml$}i || mime_type =~ %r{xml}
      # nothing
    elsif filename =~ %r{\.zip$}i || mime_type =~ %r{/zip}
      Dir.mktmpdir do |dir|
        Dir.chdir(dir) do
          File.open('the.zip', 'wb') { |f| f.write(data) }
          system('unzip -qq the.zip 2>/dev/null')
          xml = Dir.glob('*.{XML,xml}')
          if xml.size != 1
            raise ArgumentError.new(_('Um arquivo ZIP deve conter um, e apenas um, arquivo XML'))
          end
          self.data = File.read(xml.first, encoding: Encoding::ASCII_8BIT)
        end
      end
    else
      raise ArgumentError.new(_('Apenas formatos XML e ZIP são suportados; %s não') % mime_type)
    end

    process_xml
  end

  def process_xml
    document = Nokogiri::XML(data)

    # Identifier
    cv_data = document.css('CURRICULO-VITAE')
    self.identifier = cv_data.attribute('NUMERO-IDENTIFICADOR').value
    time_str = cv_data.attribute('DATA-ATUALIZACAO').value + cv_data.attribute('HORA-ATUALIZACAO').value
    self.last_update = Date.strptime(time_str, '%d%m%Y%H%M%s')

    # Personal information
    personal_info = document.css('DADOS-GERAIS')
    self.name = personal_info.attribute('NOME-COMPLETO').value

    # Abstract
    abstract = document.css('RESUMO-CV')
    self.abstract = abstract.attribute('TEXTO-RESUMO-CV-RH').value

  end

end
