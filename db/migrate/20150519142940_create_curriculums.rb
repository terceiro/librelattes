class CreateCurriculums < ActiveRecord::Migration
  def change
    create_table :curriculums do |t|
      t.string :identifier
      t.string :name
      t.text :abstract
      t.date :last_update
      t.text :data

      t.timestamps
    end
  end
end
