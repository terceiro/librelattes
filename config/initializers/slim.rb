if ENV['RAILS_ENV'] == 'development'
  Slim::Engine.set_default_options pretty: true
end
