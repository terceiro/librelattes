Rails.application.routes.draw do

  root to: 'search#index', as: 'search'
  resources :curriculums

  resources :docs

end
