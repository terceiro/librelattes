require 'test_helper'

class CurriculumTest < ActiveSupport::TestCase

  # to use fixture_file_upload
  include ActionDispatch::TestProcess

  test 'reads from xml' do
    assert_can_read 'terceiro.xml', 'application/xml'
  end

  test 'reads from lattes zip' do
    assert_can_read 'terceiro.zip', 'application/octet-stream'
  end

  test 'read from proper zip file' do
    # It seems that the .zip file produced by Lattes is not actually a proper
    # zip file. Let's make sure that we still work when they fix that, if ever.
    assert_can_read 'terceiro-proper.zip', 'application/zip'
  end

  ########################################################################
  # removing private data; if Lattes does not make it public, neither do we
  # Source: https://wwws.cnpq.br/cvlattesweb/pkg_cv_estr.termo
  ########################################################################

  # a) endereço residencial;
  # b) telefone residencial;
  test 'remove home address' do
    cv = build('terceiro.xml', 'application/xml')
    assert cv.sanitized_data !~ /<ENDERECO-RESIDENCIAL/, "Sanitized XML data should not contain home address (tag <ENDERECO-RESIDENCIAL> found)"
  end

  [
    # c) filiação;
    'NOME-DO-PAI',
    'NOME-DA-MAE',
    # d) ano de nascimento;
    'DATA-NASCIMENTO',
    # e) CPF;
    'CPF',
    # f) sexo;
    'SEXO',
    # g) cor ou raça;
    'RACA-OU-COR',
    # h) identidade;
    'NUMERO-IDENTIDADE',
    'ORGAO-EMISSOR',
    'UF-ORGAO-EMISSOR',
    'DATA-DE-EMISSAO',
    # i) passaporte e,
    'NUMERO-DO-PASSAPORTE',
  ].each do |attribute|
    test "remove #{attribute} from personal information" do
      cv = build('terceiro.xml', 'application/xml')
      assert cv.sanitized_data !~ /#{attribute}=/, "#{attribute} should not be made public"
    end
  end

  # j) endereços eletrônicos.
  test 'does not publish email' do
    cv = build('terceiro.xml', 'application/xml')
    assert cv.sanitized_data !~ /EMAIL=/, "email should not be published in XML export"
  end


  def build(filename, content_type)
    upload = fixture_file_upload(filename, content_type)
    Curriculum.build(upload)
  end

  def assert_can_read(filename, content_type)
    cv = build(filename, content_type)

    assert_equal 'Antonio Soares de Azevedo Terceiro', cv.name
    assert_equal '3828756068970319', cv.identifier
    assert_kind_of Date, cv.last_update
    assert_match /UFBA/, cv.abstract
    assert_not_nil cv.send(:data)
  end

end
