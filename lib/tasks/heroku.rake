namespace :heroku do
  desc 'Deploys to heroku'
  task :deploy do
    sh 'rake'
    branch = `git symbolic-ref --short HEAD`.strip
    deploy_branch = "tmp-#{Time.now.strftime('%Y-%m-%d_%H-%M')}"
    deploy_tag = deploy_branch.sub('tmp-', 'deploy-')
    begin
      sh 'git diff-index --quiet HEAD'
      sh 'git checkout -b ' + deploy_branch
      sh 'RAILS_ENV=production bundle --local'
      sh 'git add --force Gemfile.lock'
      sh "git commit -m \"#{deploy_tag}\""
      sh 'git push --force heroku HEAD:master'
      sh 'heroku run rake db:migrate'
      sh 'git tag ' + deploy_tag
    ensure
      sh 'git rm -f --cache Gemfile.lock'
      sh "git checkout #{branch}"
      sh "git branch -D #{deploy_branch} || true"
      sh 'bundle --local'
    end
  end
end
